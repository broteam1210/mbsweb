/*!
 * Facebook Events Calendar TinyMCE Plugin
 * @author WPPress
 */

(function() {
    tinymce.create('tinymce.plugins.FBEventCalendar', {
        init: function(ed, url) {
            var t = this;
            t.plugin_url=url;
            ed.addCommand('fbeventcal', function() {
                ed.windowManager.open({
                    file: ajaxurl + "?action=wppress_event_calendar_tinymce",
                    width: jQuery( window ).width() * 0.7,
                    height: (jQuery( window ).height() - 36 - 50) * 0.7,
                    inline: 0,
                    id: 'wppress-fb-calendar-dialog',
                }, {
                    plugin_url: url // Plugin absolute URL
                });
            });
            ed.addButton('fbeventcal', {
                title: 'Facebook Events Calendar',
                cmd: 'fbeventcal',
                image: url + '/../icon.png'
            });
        },
        createControl: function(n, cm) {
            return null;
        },
        getInfo: function() {
            return {
                longname: 'Facebook Events Calendar',
                author: 'WPPress',
                authorurl: 'https://wppress.net',
                infourl: 'https://wppress.net',
                version: "1.0"
            };
        },
    });
    function appendInsertDialog () {
        var dialogBody = jQuery( '#wppress-fb-calendar-dialog' ).append( '<span class="spinner"></span>' );

        // Get the form template from WordPress
        jQuery.post( ajaxurl, {
            action: 'wppress_event_calendar_tinymce'
        }, function( response ) {
            template = response;
            dialogBody.children( '.loading' ).remove();
            dialogBody.append( template );
            jQuery( '.spinner' ).hide();
        });
    }
    // Register plugin
    tinymce.PluginManager.add('fbeventcal', tinymce.plugins.FBEventCalendar);
})();
