<?php

/*
Plugin Name: Facebook Events Calendar
Description: Facebook Events Calendar Plugin for WordPress
Plugin URI: http://wppress.net/
Author: WPPress.net
Version: 4.4.0
Author URI: http://codecanyon.net/user/wppress
Text Domain: facebook-events
Domain Path: /languages/

*/
class WPPress_Facebook_Calendar
{
	private static $instance = null;
	private $shortcode_default = array();
	private $token_generator = "http://wppress.net/fb-token-generator/";
	private $endpoint = "https://graph.facebook.com/v2.2/";
	private $setting_name = "wppress_fb_event_calendar";
	private $timeInSeconds = array(
		'seconds' => 1,
		"minutes" => 60,
		"hours" => 3500
	);
	private $plugin_data = array(
		'Version' => false
	);
	public static function get_instance() {
		if (self::$instance == null) {
			self::$instance = new self();
		}
		return self::$instance;
	}
	function __construct() {
		global $wpdb;
		$file = __FILE__;
		$this->pluginURL = plugin_dir_url($file);
		$this->db = $wpdb;
		$this->cacheTable = $this->db->prefix . "wpp_fb_cal_cache";
		$this->timezones = include (dirname($file) . '/inc/timezones.php');
		add_action('init', array(&$this,
			'init'
		));
		add_action('admin_menu', array(&$this,
			'admin_menu'
		));
		add_action('admin_init', array(&$this,
			'admin_init'
		));
		
		add_action('wp_ajax_get_fb_cal_event', array(&$this,
			'event_json'
		));
		add_action('wp_ajax_nopriv_get_fb_cal_event', array(&$this,
			'event_json'
		));
		add_action('wp_ajax_wppress_event_calendar_tinymce', array(&$this,
			'tinymce_popup'
		));
		add_shortcode('fb-cal', array(&$this,
			'shortcode'
		));
		add_action('wp_enqueue_scripts', array(&$this,
			'wp_enqueue_scripts'
		));
		
		add_action('wp_head', array(&$this,
			'wp_head'
		));
		add_action('wp_footer', array(&$this,
			'wp_footer'
		));
		add_action('wpp_event_cal_routine', array(&$this,
			'renew_access_token'
		));
		add_action('plugins_loaded', array(&$this,
			'plugins_loaded'
		));
		add_filter('fbcal_display_modes', array(&$this,
			'display_modes'
		));
		$this->defaultOptions = array(
			'page_id' => "609242875825429",
			'access_token' => "",
			"cache_time" => 1,
			"cache_suffix" => "hour",
			"timezone" => get_option('timezone_string') ,
			'license_key' => "",
			"weekoffset"=>0,
		);
		$this->options = $this->get_option();
		
		$this->access_token = $this->options['access_token'];
		
		$this->shortcode_default = array(
			"id" => $this->options['page_id'],
			"since" => date('Y-m-d') ,
			"until" => date('Y-m-d', strtotime("+2 months")) ,
		);
	}
	public function p_url($path = "") {
		return trailingslashit($this->pluginURL) . trim($path, "/");
	}
	public function p_dir($path = "") {
		return trailingslashit(dirname(__FILE__)) . trim($path, "/");
	}
	function plugins_loaded() {
		$this->plugin_data = get_file_data(__FILE__, array(
			'Name' => 'Plugin Name',
			'PluginURI' => 'Plugin URI',
			'Version' => 'Version',
			'Description' => 'Description'
		) , 'plugin');
	}
	private function get_option() {
		$setting = get_option($this->setting_name);
		foreach ($this->defaultOptions as $key => $val) {
			if (!isset($setting[$key])) {
				$setting[$key] = $val;
			}
		}
		return $setting;
	}
	function update_options() {
		update_option($this->setting_name, $this->options);
	}
	function setup_cron() {
		if (!wp_next_scheduled('wpp_event_cal_routine')) {
			wp_schedule_event(time() , 'daily', 'wpp_event_cal_routine');
		}
	}
	function renew_access_token() {
		
		if ($this->options['access_token'] != "") {
			$this->options['access_token'] = $this->_extend_access_token($this->options['access_token']);
			$this->update_options();
		}
	}
	function admin_init() {
		if (function_exists('register_setting')) {
			register_setting('wppress-fb-event-calendar', 'wppress_fb_event_calendar');
		}
		if (is_admin() && isset($_GET['page']) && $_GET['page'] == "fb_events_calendar") {
			if (isset($_GET['access_token'])) {
				$this->access_token = $this->options['access_token'] = $_GET['access_token'];
				update_option("wppress_fb_event_calendar", $this->options);
				wp_redirect(add_query_arg(array(
					'page' => $_GET['page'],
					"access-token-acquired" => true
				) , admin_url('admin.php')));
			}
			if (isset($_GET['settings-updated'])) {
				add_action('admin_notices', array(&$this,
					'settings_updated'
				));
			}
			if (isset($_GET['access-token-acquired'])) {
				add_action('admin_notices', array(&$this,
					'token_updated'
				));
			}
			if (isset($_GET['no_access_token'])) {
				add_action('admin_notices', array(&$this,
					'token_error'
				));
			}
		}
		
		wp_enqueue_style('fbcal-tinymce-popup', $this->pluginURL . 'tinymce/style.css');
		
		$this->register_tinymce();
		$this->validate_access_token();
	}
	function _extend_access_token($token = "") {
		$url = add_query_arg(array(
			"action" => "extend_token",
			"access_token" => $this->access_token
		) , $this->token_generator);
		
		$res = wp_remote_get($url);
		if (!is_wp_error($url)) {
			$return = json_decode(wp_remote_retrieve_body($res));
			$new_access_token = $return->new_access_token;
			if ($new_access_token != "") {
				$token = $extended_token;
			}
		}
		return $token;
	}
	function install() {
	}
	function admin_menu() {
		$plugin_page = add_menu_page(__('Facebook Event Calendar', 'facebook-events') , __('Event Cal.', 'facebook-events') , 'manage_options', 'fb_events_calendar', array(&$this,
			'option'
		) , $this->pluginURL . '/icon.png');
		add_action('admin_head-' . $plugin_page, array(&$this,
			'admin_head'
		));
	}
	function admin_head() {
		echo "<script type=\"text/javascript\">";
		echo "/* */if(typeof WPPress==\"undefined\"){var WPPress={};}";
		echo "WPPress.FBCalendarURL=\"" . $this->pluginURL . "\"; /* */";
		echo "</script>";
	}
	function register_tinymce() {
		if (!current_user_can('edit_posts') && !current_user_can('edit_pages')) return;
		if (get_user_option('rich_editing') == 'true') {
			add_filter('mce_external_plugins', array(&$this,
				'add_tinymce_plugin'
			));
			add_filter('mce_buttons', array(&$this,
				'register_tinymce_buttons'
			));
		}
	}
	function add_tinymce_plugin($plugin_array) {
		$plugin_array['fbeventcal'] = $this->pluginURL . 'tinymce/editor_plugin.min.js';
		return $plugin_array;
	}
	function register_tinymce_buttons($buttons) {
		array_push($buttons, "|", "fbeventcal");
		return $buttons;
	}
	function tinymce_popup() {
		include dirname(__FILE__) . '/inc/editor-dialog.php';
		die();
	}
	
	function init() {
		load_plugin_textdomain('facebook-events', false, dirname(plugin_basename(__FILE__)) . '/languages');
		$this->install();
		$this->setup_cron();
	}
	function wp_head() {
		global $wp_locale;
		echo "<script type=\"text/javascript\">";
		echo "/* */if(typeof WPPress==\"undefined\"){var WPPress={};}";
		echo "WPPress.adminajax=\"" . admin_url('admin-ajax.php') . "\";";
		echo "WPPress.facebooEventsCalendar=\"" . trailingslashit($this->pluginURL) . "\";";
		echo "WPPress.weekoffset=".$this->options['weekoffset']. ";";
		if ($this->options['timezone'] != "USER_BROWSER") {
			echo "WPPress.FBCalTimezone=\"" . $this->options['timezone'] . "\";";
		}
		echo "WPPress.calendar={locale:{},lang:\"" . get_locale() . "\"};";
		echo "WPPress.calendar.locale.months=" . json_encode(array_values($wp_locale->month)) . ";";
		echo "WPPress.calendar.locale.monthsShort  =" . json_encode(array_values($wp_locale->month)) . ";";
		echo "WPPress.calendar.locale.weekdays =" . json_encode(array_values($wp_locale->weekday)) . ";";
		echo "WPPress.calendar.locale.weekdaysShort =" . json_encode(array_values($wp_locale->weekday_abbrev)) . ";";
		echo "WPPress.calendar.locale.weekdaysMin =" . json_encode(array_values($wp_locale->weekday_initial)) . ";";
		echo "/* */</script>";
	}
	function wp_footer() {
	}
	function wp_enqueue_scripts() {
		wp_enqueue_style('wppress-fb-calendar', $this->pluginURL . "css/fb-calendar.css", null, $this->plugin_data['Version']);
		if (file_exists(get_bloginfo('stylesheet_directory') . '/fb-calendar.css')) {
			wp_enqueue_style('wppress-fb-calendar-custom', get_bloginfo('stylesheet_directory') . '/fb-calendar.css', null, $this->plugin_data['Version']);
		}
		wp_register_script('moment', $this->pluginURL . 'js/moment.min.js', array(
			'jquery',
			'underscore'
		) , $this->plugin_data['Version'], true);
		wp_register_script('moment-timezone', $this->pluginURL . 'js/moment-timezone.min.js', array(
			'moment',
		) , $this->plugin_data['Version'], true);
		wp_register_script('CLNDR', $this->pluginURL . 'js/clndr.min.js', array(
			'moment-timezone',
		) , $this->plugin_data['Version'], true);
		wp_register_script('wppress-fb-calendar-js', $this->pluginURL . 'js/scripts.min.js', array(
			'CLNDR',
		) , $this->plugin_data['Version'], true);
		wp_enqueue_script('wppress-fb-calendar-js');
	}
	
	function option() {
		
		require dirname(__FILE__) . '/inc/options.php';
	}
	
	function settings_updated() {
		echo '<div class="updated">';
		echo '<p>' . __('Settings Updated Successfully', 'facebook-events') . '</p>';
		echo '</div>';
	}
	function token_updated() {
		echo '<div class="updated">';
		echo '<p>' . __('Access Token has been udpated successfully', 'facebook-events') . '</p>';
		echo '</div>';
	}
	function token_error() {
		echo '<div class="error">';
		echo '<p>' . __('Something went wrong while trying to acquire access token. Please again later', 'facebook-events') . '</p>';
		echo '</div>';
	}
	
	function shortcode($atts) {
		$default_atts = array(
			'id' => $this->options['page_id'],
			'include_attending' => "no",
			'show_event_calendar' => "yes",
			'show_event_cover' => "yes",
			'show_event_date' => "yes",
			'show_event_venue' => "yes",
			'hide_past_events' => "no",
			'show_event_map' => "yes",
			'show_in_popup' => "yes",
			'mode' => "calendar",
		);
		$parsed = shortcode_atts($default_atts, $atts, 'wppress-fb-event');
		extract($parsed);
		if ($id == "") {
			$id = $this->options['page_id'];
		}
		
		$modes = apply_filters('fbcal_display_modes', array());
		if (!isset($modes[$mode])) {
			$mode = 'default';
		}
		if (!file_exists($modes[$mode]['template'])) {
			$mode = "default";
		}
		$calendarID = uniqid();
		$attributes = $params = array();
		$attributes['data-user-id'] = $id;
		$attributes['data-include-attending'] = $include_attending;
		$attributes['data-show-list'] = $show_list;
		$attributes['data-calendar-step'] = $month_step;
		$attributes['data-hide-old'] = $hide_past_events;
		$attributes['data-mode'] = $mode;
		$attributes['data-show-in-popup'] = $show_in_popup;
		$attributes['data-template'] = "fb-calendar-template-" . $calendarID;
		foreach ($attributes as $key => $val) {
			$params[] = $key . '="' . esc_attr($val) . '"';
		}
		
		ob_start();
		include $modes[$mode]['template'];
		echo '<script type="text/javascript">jQuery(document).ready(function($){typeof WPPress._initFB_calendar=="function" && WPPress._initFB_calendar();});</script>';
		$content = ob_get_clean();
		return $content;
	}
	function event_json() {
		header("Content-Type: application/json");
		$ids = array_filter(explode(",", urldecode($_GET['id'])));
		$year = isset($_GET['year']) ? $_GET['year'] : date("Y");
		$month = isset($_GET['month']) ? $_GET['month'] : date("m");
		$hide_old = isset($_GET['hide_old']) ? $_GET['hide_old'] : "no";
		$this_month_start = date('Y') . "-" . date('m') . "-01T00:00:00+00:00";
		$month_start = $year . "-" . $month . "-01";
		if ($hide_old == "yes") {
			$today_date = date('Y-m-d');
			if (strtotime($this_month_start) > strtotime($month_start)) {
				
				/* If fetched date is less than current month start date, then set fetched ids to null so no events will be pulled */
				$ids = array();
			}
			if (strtotime($this_month_start) == strtotime($month_start)) {
				
				/* If fetched month is current month, try setting event start date as today to omit events before today */
				$month_start = $today_date;
			}
		}
		$until = $year . "-" . $month . "-" . cal_days_in_month(CAL_GREGORIAN, $month, $year) . "T23:59:59+00:00";
		$events = array();
		foreach ($ids as $id) {
			$events = array_merge($events, $this->_events(trim($id) , $month_start, $until));
			if ($_GET['attending'] == "yes") {
				$events = array_merge($events, $this->_attending_events(trim($id) , $month_start, $until));
			}
		}
		$json = $raw = array();
		$eventStack = array();
		foreach ($events as $event) {
			$row = array();
			$row['id'] = $event->id;
			$row['title'] = $event->name;
			$row['description'] = wpautop(make_clickable($event->description));
			$row['location'] = $event->location;
			$row['ticket_uri'] = isset($event->ticket_uri) ? $event->ticket_uri : "";
			$row['all_day_event'] = false;
			if (isset($event->owner->name) && isset($event->owner->id)) {
				$row['owner'] = array(
					'id' => $event->owner->id,
					'name' => $event->owner->name
				);
			}
			
			if (strlen($event->start_time) == "10") {
				$row['all_day_event'] = true;
			}
			$row['date'] = date('c', strtotime($event->start_time));
			if (isset($event->end_time)) {
				$row['date_end'] = date('c', strtotime($event->start_time));
			}
			$row['venue'] = array(
				'latitude' => false,
				'longitude' => false
			);
			if (isset($event->venue)) {
				$row['venue'] = $event->venue;
			}
			if (isset($event->cover)) {
				$row['cover'] = str_replace("https://", "//", $event->cover->source);
				$row['cover_offset_x'] = $event->cover->offset_x;
				$row['cover_offset_y'] = $event->cover->offset_y;
			}
			if (!in_array($event->id, $eventStack)) {
				
				/* prevents event duplication */
				$eventStack[] = $event->id;
				if (!isset($raw[strtotime($event->start_time) ])) {
					$raw[strtotime($event->start_time) ] = array();
				}
				$raw[strtotime($event->start_time) ][$event->id] = $row;
			}
		}
		ksort($raw);
		foreach ($raw as $time => $events) {
			foreach ($events as $id => $event) {
				$json[] = $event;
			}
		}
		
		$output = json_encode($json);
		if (isset($_GET['callback'])) {
			echo " /*** JSON Callback***/ ".$_GET['callback'] . "(" . $output . ");";
		} 
		else {
			echo $output;
		}
		die();
	}
	function _events($id = "", $since = false, $until = false, $after = false) {
		$params = $query = array();
		$params['api'] = "$id/events";
		$query['fields'] = "name,id,description,start_time,end_time,timezone,ticket_uri,cover,owner,venue,location";
		$query['limit'] = 25;
		$query['access_token'] = $this->access_token;
		$query['since'] = strtotime(($since ? $since : date("Y-m") . "-01"));
		$query['until'] = strtotime(($until ? $until : date("Y-m") . "-" . cal_days_in_month(CAL_GREGORIAN, date("m") , date("Y"))));
		if ($after) {
			$query['after'] = $after;
		}
		
		$params['query'] = $query;
		$data = $this->_api($params);
		$events = array();
		if (isset($data->data) && !empty($data->data) && $data != null) {
			foreach ($data->data as $event) {
				$events[] = $event;
			}
		}
		if (isset($data->paging->cursors->after)) {
			$events = array_merge($events, $this->_events($id, $since, $until, $data->paging->cursors->after));
		}
		return $events;
	}
	function _attending_events($id = "", $since = false, $until = false, $after = false) {
		$params = $query = array();
		$params['api'] = "$id/events/attending";
		$query['fields'] = "name,id,description,start_time,end_time,timezone,ticket_uri,cover,venue,location";
		$query['limit'] = 50;
		$query['access_token'] = $this->access_token;
		$query['since'] = strtotime(($since ? $since : date("Y-m") . "-01"));
		$query['until'] = strtotime(($until ? $until : date("Y-m") . "-" . cal_days_in_month(CAL_GREGORIAN, date("m") , date("Y"))));
		if ($after) {
			$query['after'] = $after;
		}
		$query['pretty'] = 0;
		$params['query'] = $query;
		$data = $this->_api($params);
		$events = array();
		if (isset($data->data) && !empty($data->data) && $data != null) {
			foreach ($data->data as $event) {
				$events[] = $event;
			}
		}
		if (isset($data->paging->cursors->after)) {
			$events = array_merge($events, $this->_attending_events($id, $since, $until, $data->paging->cursors->after));
		}
		return $events;
	}
	function display_modes($modes = array()) {
		$modes['default'] = array(
			"label" => __("Default", "facebook-events") ,
			'template' => $this->p_dir('shortcode/index.php')
		);
		$modes['list'] = array(
			"label" => __("List", "facebook-events") ,
			'template' => $this->p_dir('shortcode/list.php')
		);
		
		return $modes;
	}
	private function _api($params = array()) {
		$params['query']['pretty'] = 0;
		$url = $this->endpoint . $params['api'] . "?" . http_build_query($params['query'], $id);
		$data = $this->get_remote($url);
		return $data;
	}
	private function get_remote($url = "") {
		$timeout = $this->options['cache_time'] * $this->timeInSeconds[$this->options['cache_suffix']];
		$key = md5('wpp-fb-calendar-' . $timeout . "-" . $url);
		$cache = get_transient($key);
		if ($cache) {
			return $cache;
		}
		$result = wp_remote_get($url, array(
			'sslverify' => false,
			'blocking' => true,
		));
		if (is_wp_error($result)) {
			return false;
		}
		$data = json_decode(wp_remote_retrieve_body($result));
		set_transient($key, $data, $timeout);
		return $data;
	}
	public static function makeClickableLinks($s) {
		return preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([\w/_\.-]*(\?\S+)?)?)?)@', '<a href="$1" target="_blank">$1</a>', $s);
	}
	public function validate_access_token() {
		if ($this->options['app_id'] != "" && $this->options['app_secret'] != "") {
			$params = array(
				'api' => "oauth/access_token_info",
				"query" => array(
					'access_token' => $this->access_token
				)
			);
			$token = $this->_api($params);
			if (isset($token->error)) {
				add_action('admin_notices', array(&$this,
					'show_token_error_msg'
				));
			}
		}
	}
	public function show_token_error_msg() {
		$message = __('<b>Facebook Events Calendar</b>: The access token has expired or is invalid. Please update the access token <a href="%s"><b>here</b></a>.', 'facebook-events');
		echo "<div class=\"error\"> <p>" . sprintf($message, admin_url('admin.php?page=fb_events_calendar')) . "</p></div>";
	}
	function get_fb_userinfo() {
		if ($this->access_token == "") {
			return false;
		}
		return $this->_api(array(
			"api" => "me",
			"query" => array(
				"access_token" => $this->access_token
			)
		));
	}
}
function FBCAL() {
	return WPPress_Facebook_Calendar::get_instance();
}
FBCAL();
include dirname(__FILE__) . '/inc/widget.php';
if (file_exists(dirname(__FILE__) . '/inc/auto-update.php') && !class_exists('WPPress_Update_Manager')) {
	include dirname(__FILE__) . '/inc/auto-update.php';
}
if (class_exists("WPPress_Update_Manager")) {
	new WPPress_Update_Manager(__FILE__, "9855120", FBCAL()->options['license_key'], admin_url('admin.php?page=fb_events_calendar') , "facebook-events");
}

/* Fallback for old PHP versions*/
if (!function_exists('cal_days_in_month')) {
	function cal_days_in_month($calendar, $month, $year) {
		return date('t', mktime(0, 0, 0, $month, 1, $year));
	}
}
if (!defined('CAL_GREGORIAN')) {
	define('CAL_GREGORIAN', 1);
}
