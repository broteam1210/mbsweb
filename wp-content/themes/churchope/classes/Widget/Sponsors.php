<?php

class Widget_Sponsors extends Widget_Default
{
    /**
     * Wiget constructor
     */
    function __construct() {

        $this->setClassName( 'widget_sponsors' );
        $this->setName( __( 'Sponsors logo','churchope' ) );
        $this->setDescription( __( 'Upload sponsor logo as image file','churchope' ) );
        $this->setIdSuffix( 'sponsors' );
        parent::__construct();

        // add admin scripts
        add_action('admin_enqueue_scripts', 'sponsors_widget_script');

        function sponsors_widget_script() {
            wp_enqueue_media();
            wp_enqueue_script('ads_script', get_template_directory_uri() . '/backend/js/sponsors_widget.js', false, '1.0', true);
        }
    }

    function widget( $args, $instance ) {
        extract( $args );

        $logo_description = apply_filters('widget_title', $instance['text'] );
        ?>

        <li class="grid_3 item">
            <a href="<?php echo esc_url($instance['link_url']); ?>" target="_blank">
                <img src="<?php echo esc_url($instance['image_uri']); ?>" alt="<?php echo $logo_description; ?>"
                     title="<?php echo $logo_description; ?>" class="sponsor-logo" />
            </a>
        </li>

        <?php
    }

    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;

        $instance['text'] = strip_tags( $new_instance['text'] );
        $instance['link_url'] = strip_tags( $new_instance['link_url'] );
        $instance['image_uri'] = strip_tags( $new_instance['image_uri'] );

        return $instance;
    }


    function form( $instance ) {
        ?>

        <p>
            <label for="<?php echo $this->get_field_id('image_uri'); ?>">Image</label>
            <br />

            <img class="custom_media_image" src="<?php echo $instance['image_uri']; ?>"
                 id="image_thumbnail_<?php echo uniqid(); ?>"
                 style="margin:0;padding:0;max-width:100px;display:inline-block" />
            <br />

            <input type="hidden" name="<?php echo $this->get_field_name('image_uri'); ?>"
                   id="<?php echo 'hidden_image_id_' . uniqid(); ?>"
                   value="<?php echo $instance['image_uri']; ?>" class="widefat hidden_image_uri" />

            <input type="button" class="button button-primary custom_media_button"
                   id="custom_media_button_<?php echo uniqid(); ?>"
                   name="<?php echo $this->get_field_name('image_uri'); ?>"
                   value="Upload Image" style="margin-top:5px;" />

        </p>
        <p>
            <label for="<?php echo $this->get_field_id('text'); ?>">Description</label>
            <br />

            <input type="text" name="<?php echo $this->get_field_name('text'); ?>"
                   id="<?php echo $this->get_field_id('text'); ?>"
                   value="<?php echo $instance['text']; ?>" class="widefat" />
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('link_url'); ?>">Link url</label>
            <br />

            <input type="text" name="<?php echo $this->get_field_name('link_url'); ?>"
                   id="<?php echo $this->get_field_id('link_url'); ?>"
                   value="<?php echo $instance['link_url']; ?>" class="widefat" />
        </p>

        <?php
    }
}
?>
