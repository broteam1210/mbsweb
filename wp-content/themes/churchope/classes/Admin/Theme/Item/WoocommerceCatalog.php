<?php

/**
 * 'WooCommerce' admin menu page
 */
class Admin_Theme_Item_WoocommerceCatalog extends Admin_Theme_Menu_Item
{

	/**
	 * prefix of file icons option
	 */
	public function __construct( $parent_slug = '' ) {

		$this->setPageTitle( __( 'WooCommerce Catalog Page options', 'churchope' ) );
		$this->setMenuTitle( __( 'WooCommerce Catalog', 'churchope' ) );
		$this->setCapability( 'administrator' );
		$this->setMenuSlug( SHORTNAME . '_woocommerce_catalog' );
		$this->setIsCustomize( true );
		parent::__construct( $parent_slug );
		$this->init();
	}

	public function init() {

		$option = new Admin_Theme_Element_Pagetitle();
		$option->setName( __( 'WooCommerce  Catalog Page options', 'churchope' ) );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Select();
		$option->setName( __( 'Sidebar position for catalog page', 'churchope' ) )
				->setDescription( __( 'Choose a sidebar position for catalog page', 'churchope' ) )
				->setId( SHORTNAME . '_products_listing_layout' )
				->setStd( 'none' )
				->setOptions( array( 'none', 'left', 'right' ) );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_SelectSidebar();
		$option->setName( __( 'Sidebar for catalog page', 'churchope' ) )
				->setDescription( __( 'Choose a sidebar for catalog page', 'churchope' ) )
				->setId( SHORTNAME . '_products_listing_sidebar' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Catalog Products per Page', 'churchope' ) )
				->setDescription( __( 'Catalog Products per Page', 'churchope' ) )
				->setId( SHORTNAME . '_woo_listing_per_page' )
				->setStd( '12' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Hide Products per Page Filter', 'churchope' ) )
				->setName( __( 'Hide Products per Page Filter', 'churchope' ) )
				->setCustomized()
				->setId( SHORTNAME . '_woo_per_page_filter' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Catalog Price font size', 'churchope' ) )
				->setDescription( __( 'Widget price size at any units', 'churchope' ) )
				->setId( SHORTNAME . '_category_font_size' )
				->setStd( '16px' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Text();
		$option->setName( __( 'Catalog Price font weight', 'churchope' ) )
				->setDescription( __( 'Widget price font weight at any units', 'churchope' ) )
				->setId( SHORTNAME . '_category_font_weight' )
				->setStd( '400' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Catalog Regular Price color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color', 'churchope' ) )
				->setId( SHORTNAME . '_category_pricecolor' )
				->setStd( '#c62c02' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Colorchooser();
		$option->setName( __( 'Catalog Sale Price color', 'churchope' ) )
				->setDescription( __( 'Please select your custom color.', 'churchope' ) )
				->setId( SHORTNAME . '_category_special_pricecolor' )
				->setStd( '#758d02' );
		$this->addOption( $option );

		$option = new Admin_Theme_Element_Separator();
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Hide product rating', 'churchope' ) )
				->setName( __( 'Check to hide product rating from catalog', 'churchope' ) )
				->setCustomized()  // Show this element on WP Customize Admin menu
				->setId( SHORTNAME . '_woo_rating' );
		$this->addOption( $option );
		$option = null;

		$option = new Admin_Theme_Element_Checkbox();
		$option->setDescription( __( 'Hide "Add to cart" button', 'churchope' ) )
				->setName( __( 'Check to hide "Add to cart" button from catalog', 'churchope' ) )
				->setCustomized()  // Show this element on WP Customize Admin menu
				->setId( SHORTNAME . '_woo_add_to_cart' );
		$this->addOption( $option );
		$option = null;
	}

	/**
	 * Save form and set option-flag for reinit rewrite rules on init
	 */
	public function saveForm() {

		parent::saveForm();
		$this->setNeedReinitRulesFlag();
	}

	/**
	 * Reset form and set option-flag for reinit rewrite rules on init
	 */
	public function resetForm() {

		parent::resetForm();
		$this->setNeedReinitRulesFlag();
	}

	/**
	 * save to DB flag of need do flush_rewrite_rules on next init
	 */
	private function setNeedReinitRulesFlag() {

		update_option( SHORTNAME . '_need_flush_rewrite_rules', '1' );
	}
}

?>
