<?php

/**
 * Class for SidebarDelete Html element
 */
class Admin_Theme_Element_SidebarDelete extends Admin_Theme_Menu_Element
{

	protected $option = array(
		'type' => Admin_Theme_Menu_Element::TYPE_SIDEBAR_DELETE,
	);

	public function render() {

		ob_start();
		echo $this->getElementHeader();
		?>
        <ul class="sidebars">
			<?php
			$get_sidebar_options = Sidebar_Generator::get_sidebars();

			if ( $get_sidebar_options != '' ) {
				foreach ( $get_sidebar_options as $sidebar_id => $sidebar ) {
					if ( is_array( $sidebar ) && $sidebar['name'] ) {
						?>

						<li id="sidebar_cell_<?php echo $sidebar_id; ?>">

							<strong><?php echo $sidebar['name']; ?></strong>
							<input type="submit" name="sidebar_rm_<?php echo $sidebar_id; ?>" id="<?php echo $sidebar_id; ?>" class="button" value="<?php _e( 'Delete', 'churchope' ); ?>" />
							<img class="sidebar_rm_<?php echo $sidebar_id; ?>" style="display:none;" src="images/spinner.gif" alt="<?php _e( 'Loading', 'churchope' ); ?>" />

							<input id="<?php echo 'sidebar_generator_' . $sidebar_id ?>" type="hidden" name="<?php echo 'sidebar_generator_' . $sidebar_id ?>" value="<?php echo $sidebar['name']; ?>" />
                        </li>
						<?php
					}
				}
			}
			?>
        </ul>
		<?php
		echo $this->getElementFooter();

		$html = ob_get_clean();
		return $html;
	}
}
?>
