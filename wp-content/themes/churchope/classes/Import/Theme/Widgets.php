<?php

class Import_Theme_Widgets extends Import_Theme_Default
{
	function __construct( $type ) {

		parent::__construct( $type );
	}


	public function import_v1() {

		$sidebars = get_option( 'sidebars_widgets' );
		$sidebars['default-sidebar'] = array();
		$sidebars['header'] = array( 'churchope-nextevent-5' );
		$sidebars['footer-1'] = array( 'text-2' );
		$sidebars['footer-2'] = array( 'churchope-social-links-2' );
		$sidebars['footer-3'] = array( 'nav_menu-7' );
		$sidebars['footer-4'] = array( 'churchope-feedburner-9' );
		$sidebars['th_sidebar-1'] = array( 'text-5' );
		$sidebars['th_sidebar-2'] = array( 'nav_menu-2', 'churchope-testimonials-7' );
		$sidebars['th_sidebar-3'] = array( 'search-5', 'churchope-popular-posts-2', 'churchope-twitter-5', 'tag_cloud-2' );
		$sidebars['th_sidebar-4'] = array( 'churchope-feedburner-3', 'text-9', 'text-10' );
		$sidebars['th_sidebar-5'] = array( 'churchope-nextevent-3', 'churchope-recent-posts-4', 'churchope-testimonials-5' );
		$sidebars['th_sidebar-6'] = array( 'search-6', 'churchope-flickr-3', 'churchope-upcomingevent-2', 'churchope-contactform-3' );
		$sidebars['th_sidebar-7'] = array( 'text-13', 'churchope-twitter-2', 'text-12' );
		$sidebars['th_sidebar-8'] = array( 'churchope-recent-posts-2', 'churchope-testimonials-4' );
		$sidebars['th_sidebar-9'] = array( 'text-11', 'calendar-3', 'churchope-feedburner-6' );
		$sidebars['th_sidebar-10'] = array( 'nav_menu-3', 'churchope-testimonials-6' );
		$sidebars['th_sidebar-11'] = array( 'text-4', 'text-8', 'text-7', 'churchope-gallery-3', 'churchope-feedburner-4' );
		$sidebars['th_sidebar-12'] = array( 'churchope-flickr-2', 'churchope-upcomingevent-3', 'text-6', 'calendar-2', 'churchope-contactform-4' );
		$sidebars['th_sidebar-13'] = array( 'text-15' );
		$sidebars['th_sidebar-14'] = array( 'text-17', 'text-16' );
		$sidebars['th_sidebar-15'] = array( 'text-18' );
		$sidebars['th_sidebar-16'] = array( 'text-20', 'text-19' );
		$sidebars['th_sidebar-17'] = array( 'nav_menu-9' );
		$sidebars['th_sidebar-18'] = array( 'text-22' );
		$sidebars['th_sidebar-19'] = array( 'text-21' );
		$sidebars['th_sidebar-20'] = array( 'search-7', 'churchope-sermon-speakers-4', 'churchope-upcomingevent-5', 'churchope-feedburner-7', 'churchope-sermon-categories-3' );
		$sidebars['th_sidebar-21'] = array( 'churchope-recent-sermons-2', 'churchope-sermon-speakers-2', 'churchope-sermon-categories-2' );
		$sidebars['th_sidebar-22'] = array( 'churchope-sermon-speakers-3', 'churchope-upcomingevent-4', 'churchope-twitter-6' );
		$sidebars['th_sidebar-23'] = array( 'churchope-sermon-speakers-5', 'churchope-recent-sermons-3', 'churchope-sermon-categories-4' );
		$sidebars['th_sidebar-24'] = array( 'woocommerce_product_search-2', 'woocommerce_product_categories-2', 'woocommerce_price_filter-2', 'woocommerce_top_rated_products-2', 'churchope-feedburner-8' );
		update_option( 'sidebars_widgets', $sidebars );

		// Widget Churchope nextevent
		$churchope_nextevent = get_option( 'widget_churchope-nextevent' );
		$churchope_nextevent[3] = array( 'specific_event' => 'next', 'hide-expired' => '', 'title' => 'Next event in:', 'days' => 'DAYS', 'hr' => 'HR', 'min' => 'MIN', 'sec' => 'SEC' );
		$churchope_nextevent[5] = array( 'specific_event' => 'next', 'hide-expired' => '', 'title' => 'Next event in:', 'days' => 'DAYS', 'hr' => 'HR', 'min' => 'MIN', 'sec' => 'SEC' );
		$churchope_nextevent['_multiwidget'] = 1;
		update_option( 'widget_churchope-nextevent', $churchope_nextevent );

		// Widget Text
		$text = get_option( 'widget_text' );
		$text[2] = array( 'title' => 'When and Where?', 'text' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, <br/><br/> Coffee Club &ndash; 10:00a<br/> Worship &ndash; 10:30a<br/> AM Exchange &ndash; 11:30<br/> <br/> [button type="simple_button_link" url="http://themoholics.com/" target="" ]more info[/button]', 'filter' => '' );
		$text[4] = array( 'title' => 'SOCIAL ICONS', 'text' => '[social_link type="facebook_account" url="http://www.facebook.com" target="on" ] [social_link type="rss_feed" url="#" target="" ] [social_link type="twitter" url="#" target="on" ] [social_link type="dribble_account" url="#" target="on" ] [social_link type="email_to" url="#" target="" ] [social_link type="google_plus_account" url="#" target="" ] [social_link type="flicker_account" url="#" target="" ] [social_link type="vimeo_account" url="#" target="" ]', 'filter' => '' );
		$text[5] = array( 'title' => '', 'text' => ' [three_fourth]<h2>Latest Sermon: Not a Fan: The Open Invitation - February 12, 2012</h2> [/three_fourth] [one_fourth last=last] [button type="churchope_button" url="http://themeforest.net/item/churchope-responsive-wordpress-theme/2708562" target="" ] <i class="fa fa-cloud-download"></i> &nbsp; Download Latest Sermon[/button] [/one_fourth] ', 'filter' => '' );
		$text[6] = array( 'title' => '', 'text' => '<iframe width="100%" height="120" frameborder="0" src="http://player.vimeo.com/video/35108500?title=0&amp;byline=0&amp;portrait=0"></iframe>', 'filter' => '' );
		$text[7] = array( 'title' => 'Toggles', 'text' => '[toggle type="white" title="At vero eos et accusamus"]But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth[/toggle] [toggle type="white" title="Sed ut perspiciatis"]But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth[/toggle] [toggle type="white" title="On the other hand"]But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system, and expound the actual teachings of the great explorer of the truth, the master-builder of human happiness. No one rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because those who do not know how to pursue pleasure rationally encounter pleasure[/toggle]', 'filter' => '' );
		$text[8] = array( 'title' => 'Tabs', 'text' => '[tabgroup] [tab title="Nullam"]Cras at nisi nec purus tincidunt suscipit. Proin vitae dui erat. Morbi at sodales orci. Nullam id massa ornare libero luctus molestie eu quis arcu. Pellentesque interdum, nunc ut consequat posuere, turpis risus lobortis ligula, nec tempor enim velit vel augue.[/tab] [tab title="Morbi"] Nam feugiat felis id ipsum blandit sodales. Aenean vitae tortor ultricies est molestie euismod sit amet non leo. Ut velit ipsum, ullamcorper eu condimentum eget, aliquam ut lorem. Integer non velit nulla, in ornare lectus. Cras eget nunc in eros mollis posuere in eu dui. Donec facilisis, orci at sagittis pretium, libero urna consectetur elit, ac ornare odio mi sed odio. Aliquam ut risus neque, eu fringilla neque. Suspendisse potenti. Integer in diam congue diam fermentum cursus at eu lorem. [/tab] [tab title="DonecÂ "]Aenean iaculis turpis non neque molestie cursus. Donec convallis tincidunt erat, sed euismod mauris varius in. Cras ultrices sodales iaculis. Ut sodales nunc ut lectus mattis vitae sagittis lectus aliquet. Ut accumsan ligula ut augue pharetra vulputate. Fusce sit amet ligula at urna tristique cursus.Â [/tab] [/tabgroup]', 'filter' => '' );
		$text[9] = array( 'title' => '', 'text' => '[tabgroup] [tab title="Nullam"]Cras at nisi nec purus tincidunt suscipit. Proin vitae dui erat. Morbi at sodales orci. Nullam id massa ornare libero luctus molestie eu quis arcu. Pellentesque interdum, nunc ut consequat posuere, turpis risus lobortis ligula, nec tempor enim velit vel augue.[/tab] [tab title="Morbi"] Nam feugiat felis id ipsum blandit sodales. Aenean vitae tortor ultricies est molestie euismod sit amet non leo. Ut velit ipsum, ullamcorper eu condimentum eget, aliquam ut lorem. Integer non velit nulla, in ornare lectus. Cras eget nunc in eros mollis posuere in eu dui. Donec facilisis, orci at sagittis pretium, libero urna consectetur elit, ac ornare odio mi sed odio. Aliquam ut risus neque, eu fringilla neque. Suspendisse potenti. Integer in diam congue diam fermentum cursus at eu lorem. [/tab] [tab title="DonecÂ "]Aenean iaculis turpis non neque molestie cursus. Donec convallis tincidunt erat, sed euismod mauris varius in. Cras ultrices sodales iaculis. Ut sodales nunc ut lectus mattis vitae sagittis lectus aliquet. Ut accumsan ligula ut augue pharetra vulputate. Fusce sit amet ligula at urna tristique cursus.Â [/tab] [/tabgroup]', 'filter' => '' );
		$text[10] = array( 'title' => '', 'text' => '[social_link type="facebook_account" url="http://www.facebook.com" target="on" ] [social_link type="rss_feed" url="#" target="" ] [social_link type="twitter" url="#" target="on" ] [social_link type="dribble_account" url="#" target="on" ] [social_link type="email_to" url="#" target="" ] [social_link type="google_plus_account" url="#" target="" ] [social_link type="flicker_account" url="#" target="" ] [social_link type="vimeo_account" url="#" target="" ]', 'filter' => '' );
		$text[11] = array( 'title' => 'Sound shortcode', 'text' => "[thaudio href='http://churchope.themoholics.com/wp-content/uploads/2012/07/Sunny-Morning-2.mp3']Sunny Morning[/thaudio] \"But I must explain to you how all this mistaken idea of denouncing pleasure and praising pain was born and I will give you a complete account of the system...", 'filter' => '' );
		$text[12] = array( 'title' => 'Lets Get Social!', 'text' => '[social_link type="facebook_account" url="http://www.facebook.com" target="on" ] [social_link type="rss_feed" url="#" target="" ] [social_link type="twitter" url="#" target="on" ] [social_link type="dribble_account" url="#" target="on" ] [social_link type="email_to" url="#" target="" ] [social_link type="google_plus_account" url="#" target="" ] [social_link type="flicker_account" url="#" target="" ] [social_link type="vimeo_account" url="#" target="" ]', 'filter' => '' );
		$text[13] = array( 'title' => 'Video From our Latest Events', 'text' => '<iframe width="100%" height="120" frameborder="0" src="http://player.vimeo.com/video/35108500?title=0&amp;byline=0&amp;portrait=0"></iframe> <br/> <iframe width="100%" height="120" frameborder="0" src="http://player.vimeo.com/video/35439665?color=fedf91"></iframe> ', 'filter' => '' );
		$text[15] = array( 'title' => '', 'text' => " [three_fourth]<h2>Don't you think that this theme is awesome?!</h2> [/three_fourth] [one_fourth last=last] [button type=\"churchope_button\" url=\"http://themeforest.net/item/churchope-responsive-wordpress-theme/2708562?ref=themoholics\" target=\"\" ]<i class=\"fa fa-hand-o-right\"></i> &nbsp; Get this theme now![/button] [/one_fourth] ", 'filter' => '' );
		$text[16] = array( 'title' => 'Follow Us', 'text' => '[social_link type="facebook_account" url="http://www.facebook.com" target="on" ] [social_link type="rss_feed" url="#" target="" ] [social_link type="twitter" url="#" target="on" ] [social_link type="dribble_account" url="#" target="on" ] [social_link type="email_to" url="#" target="" ] [social_link type="google_plus_account" url="#" target="" ] [social_link type="flicker_account" url="#" target="" ] [social_link type="vimeo_account" url="#" target="" ]', 'filter' => '' );
		$text[17] = array( 'title' => 'Address', 'text' => '270 Potrero Avenue<br/> San Francisco, CA 94103,<br/> United States<br/> <br/> <a href="#">mail@churchope.com</a><br/> Phone: 800-321-6543', 'filter' => '' );
		$text[18] = array( 'title' => '', 'text' => ' [two_third]<h2>YOU CAN ADD SOCIAL ICONS OR OTHER CONTENT HERE</h2> [/two_third] [one_third last=last] [social_link type="facebook_account" url="http://www.facebook.com" target="on" ] [social_link type="rss_feed" url="#" target="" ] [social_link type="twitter" url="#" target="on" ] [social_link type="dribble_account" url="#" target="on" ] [social_link type="email_to" url="#" target="" ] [social_link type="google_plus_account" url="#" target="" ][/one_third] ', 'filter' => '' );
		$text[19] = array( 'title' => 'Or here!', 'text' => '[social_link type="facebook_account" url="http://www.facebook.com" target="on" ] [social_link type="rss_feed" url="#" target="" ] [social_link type="twitter" url="#" target="on" ] [social_link type="dribble_account" url="#" target="on" ] [social_link type="email_to" url="#" target="" ] [social_link type="google_plus_account" url="#" target="" ] [social_link type="flicker_account" url="#" target="" ] [social_link type="vimeo_account" url="#" target="" ]', 'filter' => '' );
		$text[20] = array( 'title' => 'Address', 'text' => '270 Potrero Avenue<br/> San Francisco, CA 94103,<br/> United States<br/> <br/> <a href="#">mail@churchope.com</a><br/> Phone: 800-321-6543', 'filter' => '' );
		$text[21] = array( 'title' => 'cal', 'text' => '[event category="38" layout="full"]', 'filter' => '' );
		$text[22] = array( 'title' => '', 'text' => " [three_fourth]<h2>Don't you think that this theme is awesome?!</h2> [/three_fourth] [one_fourth last=last] [button type=\"churchope_button\" url=\"http://themeforest.net/item/churchope-responsive-wordpress-theme/2708562?ref=themoholics\" target=\"\" button_color_fon=\"#2fc8ac\" ]<i class=\"fa fa-hand-o-right\"></i> &nbsp; Get this theme now![/button] [/one_fourth] ", 'filter' => '' );
		$text['_multiwidget'] = 1;
		update_option( 'widget_text', $text );

		// Widget Churchope social links
		$churchope_social_links = get_option( 'widget_churchope-social-links' );
		$churchope_social_links[2] = array( 'hide_icon' => '', 'title' => 'Follow us', 'twitter_account' => 'themoholics', 'twitter_account_title' => 'Twitter', 'facebook_account' => 'themoholics', 'facebook_account_title' => 'Facebook', 'google_plus_account' => 'themoholics', 'google_plus_account_title' => 'Google Plus', 'rss_feed' => '', 'rss_feed_title' => '', 'email_to' => 'themoholics', 'email_to_title' => 'Email', 'flicker_account' => '', 'flicker_account_title' => '', 'vimeo_account' => '', 'vimeo_account_title' => '', 'youtube_account' => '', 'youtube_account_title' => '', 'dribble_account' => 'themoholics', 'dribble_account_title' => 'Dribbble', 'linked_in_account' => '', 'linked_in_account_title' => '', 'pinterest_account' => '', 'pinterest_account_title' => '' );
		$churchope_social_links['_multiwidget'] = 1;
		update_option( 'widget_churchope-social-links', $churchope_social_links );

		// Widget Nav_menu
		$nav_menu = get_option( 'widget_nav_menu' );
		global $wpdb;
		$table_db_name = $wpdb->prefix . 'terms';
		$rows = $wpdb->get_results( 'SELECT * FROM ' . $table_db_name . " where name='Features' OR name='Shortcodes' OR name='FooterWidgetMenu2' OR name='Docs'", ARRAY_A );
		$menu_ids = array();
		foreach ( $rows as $row ) {
			$menu_ids[ $row['name'] ] = $row['term_id']; }

		$nav_menu[2] = array( 'title' => '', 'nav_menu' => $menu_ids['Features'] );
		$nav_menu[3] = array( 'title' => '', 'nav_menu' => $menu_ids['Shortcodes'] );
		$nav_menu[7] = array( 'title' => 'Lots of Shortcodes', 'nav_menu' => $menu_ids['FooterWidgetMenu2'] );
		$nav_menu[9] = array( 'title' => 'Online Documentation', 'nav_menu' => $menu_ids['Docs'] );
		$nav_menu['_multiwidget'] = 1;
		update_option( 'widget_nav_menu', $nav_menu );

		// Widget Churchope feedburner
		$churchope_feedburner = get_option( 'widget_churchope-feedburner' );
		$churchope_feedburner[3] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[4] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[6] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[7] = array( 'title' => 'Stay Tuned!', 'description' => 'Receive information about about latest sermons...', 'feedname' => 'themoholics' );
		$churchope_feedburner[8] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[9] = array( 'title' => 'Sing Up For Newsletter', 'description' => 'Keep up with the latest news, sales and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner['_multiwidget'] = 1;
		update_option( 'widget_churchope-feedburner', $churchope_feedburner );

		// Widget Churchope testimonials
		$churchope_testimonials = get_option( 'widget_churchope-testimonials' );
		$churchope_testimonials[4] = array( 'category' => 'testimonials', 'effect' => 'fade', 'randomize' => 'on', 'time' => '10', 'title' => 'Testimonials' );
		$churchope_testimonials[5] = array( 'category' => 'all', 'effect' => 'fade', 'randomize' => '', 'time' => '10', 'title' => 'Testimonials' );
		$churchope_testimonials[6] = array( 'category' => 'all', 'effect' => 'fade', 'randomize' => '', 'time' => '10', 'title' => 'What people say' );
		$churchope_testimonials[7] = array( 'category' => 'all', 'effect' => 'fade', 'randomize' => '', 'time' => '10', 'title' => 'What people say' );
		$churchope_testimonials['_multiwidget'] = 1;
		update_option( 'widget_churchope-testimonials', $churchope_testimonials );

		// Widget Search
		$search = get_option( 'widget_search' );
		$search[5] = array( 'title' => '' );
		$search[6] = array( 'title' => '' );
		$search[7] = array( 'title' => '' );
		$search['_multiwidget'] = 1;
		update_option( 'widget_search', $search );

		// Widget Churchope popular posts
		$churchope_popular_posts = get_option( 'widget_churchope-popular-posts' );
		$churchope_popular_posts[2] = array( 'title' => 'Popular post', 'number' => '3' );
		$churchope_popular_posts['_multiwidget'] = 1;
		update_option( 'widget_churchope-popular-posts', $churchope_popular_posts );

		// Widget Churchope twitter
		$churchope_twitter = get_option( 'widget_churchope-twitter' );
		$churchope_twitter[2] = array( 'title' => 'Twitter', 'username' => 'themoholics', 'num' => '3', 'update' => 'on', 'linked' => '', 'hyperlinks' => 'on', 'twitter_users' => 'on', 'encode_utf8' => 'on' );
		$churchope_twitter[5] = array( 'title' => 'Twitter', 'username' => 'themoholics', 'num' => '3', 'update' => 'on', 'linked' => '', 'hyperlinks' => 'on', 'twitter_users' => 'on', 'encode_utf8' => 'on' );
		$churchope_twitter[6] = array( 'title' => 'Stay Tuned!', 'username' => 'themoholics', 'num' => '3', 'update' => 'on', 'linked' => '', 'hyperlinks' => 'on', 'twitter_users' => 'on', 'encode_utf8' => 'on', 'target_blank' => '' );
		$churchope_twitter['_multiwidget'] = 1;
		update_option( 'widget_churchope-twitter', $churchope_twitter );

		// Widget Tag_cloud
		$tag_cloud = get_option( 'widget_tag_cloud' );
		$tag_cloud[2] = array( 'title' => 'Tags', 'taxonomy' => 'post_tag' );
		$tag_cloud['_multiwidget'] = 1;
		update_option( 'widget_tag_cloud', $tag_cloud );

		// Widget Churchope recent posts
		$churchope_recent_posts = get_option( 'widget_churchope-recent-posts' );
		$churchope_recent_posts[2] = array( 'title' => 'Recent posts', 'number' => '5', 'category' => '' );
		$churchope_recent_posts[4] = array( 'title' => 'Recent posts', 'number' => '3', 'category' => '' );
		$churchope_recent_posts['_multiwidget'] = 1;
		update_option( 'widget_churchope-recent-posts', $churchope_recent_posts );

		// Widget Churchope flickr
		$churchope_flickr = get_option( 'widget_churchope-flickr' );
		$churchope_flickr[2] = array( 'title' => 'Flickr', 'number' => '6', 'user' => '36587311@N08' );
		$churchope_flickr[3] = array( 'title' => 'From Flickr', 'number' => '6', 'user' => '36587311@N08' );
		$churchope_flickr['_multiwidget'] = 1;
		update_option( 'widget_churchope-flickr', $churchope_flickr );

		// Widget Churchope upcomingevent
		$churchope_upcomingevent = get_option( 'widget_churchope-upcomingevent' );
		$churchope_upcomingevent[2] = array( 'title' => 'Upcoming events', 'count' => '3', 'category' => 'all', 'phone' => 'on', 'time' => 'on', 'place' => 'on', 'email' => '' );
		$churchope_upcomingevent[3] = array( 'title' => 'Upcoming events', 'count' => '3', 'category' => 'all', 'phone' => 'on', 'time' => 'on', 'place' => 'on', 'email' => '' );
		$churchope_upcomingevent[4] = array( 'title' => 'Upcoming events', 'count' => '3', 'category' => 'all', 'phone' => 'on', 'time' => 'on', 'place' => 'on', 'email' => '' );
		$churchope_upcomingevent[5] = array( 'title' => 'Upcoming events', 'count' => '3', 'category' => 'all', 'phone' => 'on', 'time' => 'on', 'place' => 'on', 'email' => '' );
		$churchope_upcomingevent['_multiwidget'] = 1;
		update_option( 'widget_churchope-upcomingevent', $churchope_upcomingevent );

		// Widget Churchope contactform
		$churchope_contactform = get_option( 'widget_churchope-contactform' );
		$churchope_contactform[3] = array( 'title' => 'Contact us' );
		$churchope_contactform[4] = array( 'title' => 'Contact us' );
		$churchope_contactform['_multiwidget'] = 1;
		update_option( 'widget_churchope-contactform', $churchope_contactform );

		// Widget Calendar
		$calendar = get_option( 'widget_calendar' );
		$calendar[2] = array( 'title' => '' );
		$calendar[3] = array( 'title' => '' );
		$calendar['_multiwidget'] = 1;
		update_option( 'widget_calendar', $calendar );

		// Widget Churchope gallery
		$churchope_gallery = get_option( 'widget_churchope-gallery' );
		$churchope_gallery[3] = array( 'title' => 'From gallery', 'number' => '4', 'category' => 'biggallery' );
		$churchope_gallery['_multiwidget'] = 1;
		update_option( 'widget_churchope-gallery', $churchope_gallery );

		// Widget Churchope sermon speakers
		$churchope_sermon_speakers = get_option( 'widget_churchope-sermon-speakers' );
		$churchope_sermon_speakers[2] = array( 'title' => 'Speakers', 'selected_speakers' => '' );
		$churchope_sermon_speakers[3] = array( 'title' => 'Special Guests', 'selected_speakers' => '' );
		$churchope_sermon_speakers[4] = array( 'title' => 'Featured Speakers', 'selected_speakers' => '' );
		$churchope_sermon_speakers[5] = array( 'title' => 'Featured Sermons Contributors', 'selected_speakers' => '' );
		$churchope_sermon_speakers['_multiwidget'] = 1;
		update_option( 'widget_churchope-sermon-speakers', $churchope_sermon_speakers );

		// Widget Churchope sermon categories
		$churchope_sermon_categories = get_option( 'widget_churchope-sermon-categories' );
		$churchope_sermon_categories[2] = array( 'title' => 'Semon Categories', 'count' => '0', 'hierarchical' => '0', 'dropdown' => '0' );
		$churchope_sermon_categories[3] = array( 'title' => 'Sermons Categories', 'count' => '0', 'hierarchical' => '0', 'dropdown' => '0' );
		$churchope_sermon_categories[4] = array( 'title' => 'Sermons Categories', 'count' => '0', 'hierarchical' => '0', 'dropdown' => '0' );
		$churchope_sermon_categories['_multiwidget'] = 1;
		update_option( 'widget_churchope-sermon-categories', $churchope_sermon_categories );

		// Widget Churchope recent sermons
		$churchope_recent_sermons = get_option( 'widget_churchope-recent-sermons' );
		$churchope_recent_sermons[2] = array( 'title' => 'Recent sermons', 'number' => '3', 'category' => '' );
		$churchope_recent_sermons[3] = array( 'title' => 'Recent sermons', 'number' => '3', 'category' => '' );
		$churchope_recent_sermons['_multiwidget'] = 1;
		update_option( 'widget_churchope-recent-sermons', $churchope_recent_sermons );

		// Widget Woocommerce_product_search
		$woocommerce_product_search = get_option( 'widget_woocommerce_product_search' );
		$woocommerce_product_search[2] = array( 'title' => '' );
		$woocommerce_product_search['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_product_search', $woocommerce_product_search );

		// Widget Woocommerce_product_categories
		$woocommerce_product_categories = get_option( 'widget_woocommerce_product_categories' );
		$woocommerce_product_categories[2] = array( 'title' => 'Product Categories', 'orderby' => 'order', 'dropdown' => '0', 'count' => '0', 'hierarchical' => '1', 'show_children_only' => '0' );
		$woocommerce_product_categories['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_product_categories', $woocommerce_product_categories );

		// Widget Woocommerce_price_filter
		$woocommerce_price_filter = get_option( 'widget_woocommerce_price_filter' );
		$woocommerce_price_filter[2] = array( 'title' => 'Filter by price' );
		$woocommerce_price_filter['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_price_filter', $woocommerce_price_filter );

		// Widget Woocommerce_top_rated_products
		$woocommerce_top_rated_products = get_option( 'widget_woocommerce_top_rated_products' );
		$woocommerce_top_rated_products[2] = array( 'title' => 'Top Rated Products', 'number' => '3' );
		$woocommerce_top_rated_products['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_top_rated_products', $woocommerce_top_rated_products );
	}

	public function import_v2() {

		$sidebars = get_option( 'sidebars_widgets' );
		$sidebars['default-sidebar'] = array( 'search-2', 'recent-comments-2', 'archives-2', 'categories-2', 'meta-2' );
		$sidebars['header'] = array( 'churchope-nextevent-2' );
		$sidebars['footer-1'] = array( 'text-2' );
		$sidebars['footer-2'] = array();
		$sidebars['footer-3'] = array();
		$sidebars['footer-4'] = array();
		$sidebars['th_sidebar-1'] = array( 'text-3' );
		$sidebars['th_sidebar-2'] = array( 'woocommerce_product_search-3', 'woocommerce_product_categories-2', 'woocommerce_price_filter-2', 'woocommerce_top_rated_products-3', 'churchope-feedburner-8' );
		$sidebars['th_sidebar-3'] = array( 'search-3', 'churchope-popular-posts-2', 'text-4', 'churchope-feedburner-3', 'churchope-twitter-2', 'tag_cloud-2' );
		$sidebars['th_sidebar-4'] = array( 'churchope-nextevent-4', 'churchope-popular-posts-3', 'churchope-feedburner-5', 'churchope-twitter-3', 'tag_cloud-3' );
		$sidebars['th_sidebar-5'] = array( 'woocommerce_product_search-2', 'woocommerce_top_rated_products-2', 'churchope-upcomingevent-3', 'churchope-feedburner-7' );
		$sidebars['th_sidebar-6'] = array( 'churchope-nextevent-3', 'churchope-feedburner-2' );
		$sidebars['th_sidebar-8'] = array( 'text-6', 'churchope-feedburner-4', 'churchope-upcomingevent-2' );
		$sidebars['th_sidebar-9'] = array( 'text-5' );
		update_option( 'sidebars_widgets', $sidebars );

		// Widget Search
		$search = get_option( 'widget_search' );
		$search[2] = array( 'title' => '' );
		$search[3] = array( 'title' => '' );
		$search['_multiwidget'] = 1;
		update_option( 'widget_search', $search );

		// Widget Recent comments
		$recent_comments = get_option( 'widget_recent-comments' );
		$recent_comments[2] = array( 'title' => '', 'number' => '5' );
		$recent_comments['_multiwidget'] = 1;
		update_option( 'widget_recent-comments', $recent_comments );

		// Widget Archives
		$archives = get_option( 'widget_archives' );
		$archives[2] = array( 'title' => '', 'count' => '0', 'dropdown' => '0' );
		$archives['_multiwidget'] = 1;
		update_option( 'widget_archives', $archives );

		// Widget Categories
		$categories = get_option( 'widget_categories' );
		$categories[2] = array( 'title' => '', 'count' => '0', 'hierarchical' => '0', 'dropdown' => '0' );
		$categories['_multiwidget'] = 1;
		update_option( 'widget_categories', $categories );

		// Widget Meta
		$meta = get_option( 'widget_meta' );
		$meta[2] = array( 'title' => '' );
		$meta['_multiwidget'] = 1;
		update_option( 'widget_meta', $meta );

		// Widget Churchope nextevent
		$churchope_nextevent = get_option( 'widget_churchope-nextevent' );
		$churchope_nextevent[1] = array();
		$churchope_nextevent[2] = array( 'event_category' => 'all', 'specific_event' => 'next', 'hide-expired' => '', 'title' => "Don't Miss! Next Sale In:", 'days' => 'DAYS', 'hr' => 'HR', 'min' => 'MIN', 'sec' => 'SEC' );
		$churchope_nextevent[3] = array( 'event_category' => 'all', 'specific_event' => 'next', 'hide-expired' => '', 'title' => 'Next event in:', 'days' => 'DAYS', 'hr' => 'HR', 'min' => 'MIN', 'sec' => 'SEC' );
		$churchope_nextevent[4] = array( 'event_category' => 'all', 'specific_event' => 'next', 'hide-expired' => '', 'title' => "Don't Miss Our Sales!", 'days' => 'DAYS', 'hr' => 'HR', 'min' => 'MIN', 'sec' => 'SEC' );
		$churchope_nextevent['_multiwidget'] = 1;
		update_option( 'widget_churchope-nextevent', $churchope_nextevent );

		// Widget Text
		$text = get_option( 'widget_text' );
		$text[1] = array();
		$text[2] = array( 'title' => '', 'text' => '<p style="margin:10px 0"></p> <p style="text-align:center"> <img src="http://churchope.themoholics.com/v1/wp-content/uploads/2015/09/logo_chr.png"/></p> <p style="margin:5px 0"></p> <p style="text-align:center">[social_link type="twitter" url="" target="" ] [social_link type="facebook_account" url="" target="" ] [social_link type="email_to" url="" target="" ] [social_link type="pinterest_account" url="" target="" ] [social_link type="instagram_account" url="" target="" ]</p> <p style="margin:5px 0"></p> <p style="text-align:center">The standard chunk of Lorem Ipsum used since the 1500s <br> is reproduced below for those interested</p> <p style="margin:20px 0"></p> ', 'filter' => '' );
		$text[3] = array( 'title' => '', 'text' => ' [three_fourth]<img src="http://churchope.themoholics.com/v1/wp-content/uploads/2015/09/n1.png" style="float:left; margin-right:20px;"/><h2 style=" margin: 12px;">Selling Theme in Nonprofit Category of All Time! </h2> [/three_fourth] [one_fourth last=last] <p style="text-align:right;">[button type="churchope_button" button_color_fon="#ff614d" url="http://themeforest.net/item/churchope-responsive-wordpress-theme/2708562?ref=themoholics" target="" ]<i class="fa fa-shopping-cart"></i> &nbsp; Get this theme now![/button] </p>[/one_fourth] ', 'filter' => '' );
		$text[4] = array( 'title' => '', 'text' => '[rev_slider alias="sidebar"]', 'filter' => '' );
		$text[5] = array( 'title' => '', 'text' => '[one_third]<h4 style="font-weight:700;padding: 30px 0 30px 100px;margin: 0;position: relative;"><span class="fa-stack fa-3x" style="position: absolute;font-size: 42px;left: 0;top: 50%;margin-top: -42px;"> <i class="fa fa-circle-thin fa-stack-2x" style="color:#ff614d;"></i> <i style="font-size:16px; color:#545454" class="fa fa-truck fa-stack-1x"></i></span> Free Shipping on Orders Over $100</h4> [/one_third] [one_third]<h4 style="font-weight:700;padding: 30px 0 30px 100px;margin: 0;position: relative;"><span class="fa-stack fa-3x" style="position: absolute;font-size: 42px;left: 0;top: 50%;margin-top: -42px;"> <i class="fa fa-circle-thin fa-stack-2x" style="color:#ff614d;"></i> <i style="font-size:16px; color:#545454" class="fa fa-undo fa-stack-1x"></i></span> Hassle-Free" Return Policy</h4> [/one_third] [one_third last=last]<h4 style="font-weight:700;padding: 30px 0 30px 100px;margin: 0;position: relative;"><span class="fa-stack fa-3x" style="position: absolute;font-size: 42px;left: 0;top: 50%;margin-top: -42px;"> <i class="fa fa-circle-thin fa-stack-2x" style="color:#ff614d;"></i> <i style="font-size:16px; color:#545454" class="fa fa-phone fa-stack-1x"></i></span> Call Us! Toll Free 1 800 123 4567 </h4> [/one_third] ', 'filter' => '' );
		$text[6] = array( 'title' => '', 'text' => '[rev_slider alias="sidebar"] <p style="margin:10px 0"></p> ', 'filter' => '' );
		$text['_multiwidget'] = 1;
		update_option( 'widget_text', $text );

		// Widget Woocommerce_product_search
		$woocommerce_product_search = get_option( 'widget_woocommerce_product_search' );
		$woocommerce_product_search[1] = array();
		$woocommerce_product_search[2] = array( 'title' => '' );
		$woocommerce_product_search[3] = array( 'title' => '' );
		$woocommerce_product_search['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_product_search', $woocommerce_product_search );

		// Widget Woocommerce_product_categories
		$woocommerce_product_categories = get_option( 'widget_woocommerce_product_categories' );
		$woocommerce_product_categories[1] = array();
		$woocommerce_product_categories[2] = array( 'title' => 'Categories', 'orderby' => 'order', 'dropdown' => '0', 'count' => '0', 'hierarchical' => '1', 'show_children_only' => '0' );
		$woocommerce_product_categories['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_product_categories', $woocommerce_product_categories );

		// Widget Woocommerce_price_filter
		$woocommerce_price_filter = get_option( 'widget_woocommerce_price_filter' );
		$woocommerce_price_filter[1] = array();
		$woocommerce_price_filter[2] = array( 'title' => 'Filter by price' );
		$woocommerce_price_filter['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_price_filter', $woocommerce_price_filter );

		// Widget Woocommerce_top_rated_products
		$woocommerce_top_rated_products = get_option( 'widget_woocommerce_top_rated_products' );
		$woocommerce_top_rated_products[1] = array();
		$woocommerce_top_rated_products[2] = array( 'title' => 'Top Rated Products', 'number' => '3' );
		$woocommerce_top_rated_products[3] = array( 'title' => 'Top Rated Products', 'number' => '3' );
		$woocommerce_top_rated_products['_multiwidget'] = 1;
		update_option( 'widget_woocommerce_top_rated_products', $woocommerce_top_rated_products );

		// Widget Churchope feedburner
		$churchope_feedburner = get_option( 'widget_churchope-feedburner' );
		$churchope_feedburner[1] = array();
		$churchope_feedburner[2] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[3] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[4] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[5] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[7] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner[8] = array( 'title' => 'Sign up for our Newsletter', 'description' => 'Keep up with the latest news and events.', 'feedname' => 'themoholics' );
		$churchope_feedburner['_multiwidget'] = 1;
		update_option( 'widget_churchope-feedburner', $churchope_feedburner );

		// Widget Churchope popular posts
		$churchope_popular_posts = get_option( 'widget_churchope-popular-posts' );
		$churchope_popular_posts[1] = array();
		$churchope_popular_posts[2] = array( 'title' => 'Popular posts', 'number' => '3' );
		$churchope_popular_posts[3] = array( 'title' => 'Popular posts', 'number' => '3' );
		$churchope_popular_posts['_multiwidget'] = 1;
		update_option( 'widget_churchope-popular-posts', $churchope_popular_posts );

		// Widget Churchope twitter
		$churchope_twitter = get_option( 'widget_churchope-twitter' );
		$churchope_twitter[1] = array();
		$churchope_twitter[2] = array( 'title' => '', 'username' => '', 'num' => '', 'update' => '', 'linked' => '', 'hyperlinks' => '', 'twitter_users' => '', 'encode_utf8' => '', 'target_blank' => '' );
		$churchope_twitter[3] = array( 'title' => 'From Twitter', 'username' => 'themoholics', 'num' => '3', 'update' => 'on', 'linked' => '', 'hyperlinks' => 'on', 'twitter_users' => 'on', 'encode_utf8' => 'on', 'target_blank' => '' );
		$churchope_twitter['_multiwidget'] = 1;
		update_option( 'widget_churchope-twitter', $churchope_twitter );

		// Widget Tag_cloud
		$tag_cloud = get_option( 'widget_tag_cloud' );
		$tag_cloud[1] = array();
		$tag_cloud[2] = array( 'title' => 'Fashion Cloud', 'taxonomy' => 'post_tag' );
		$tag_cloud[3] = array( 'title' => 'Fashion Tags', 'taxonomy' => 'post_tag' );
		$tag_cloud['_multiwidget'] = 1;
		update_option( 'widget_tag_cloud', $tag_cloud );

		// Widget Churchope upcomingevent
		$churchope_upcomingevent = get_option( 'widget_churchope-upcomingevent' );
		$churchope_upcomingevent[1] = array();
		$churchope_upcomingevent[2] = array( 'title' => 'Upcoming Events', 'count' => '3', 'category' => 'all', 'phone' => 'on', 'time' => 'on', 'place' => 'on', 'email' => '' );
		$churchope_upcomingevent[3] = array( 'title' => 'Upcoming Sales', 'count' => '3', 'category' => 'all', 'phone' => 'on', 'time' => 'on', 'place' => 'on', 'email' => '' );
		$churchope_upcomingevent['_multiwidget'] = 1;
		update_option( 'widget_churchope-upcomingevent', $churchope_upcomingevent );
	}
}

?>
