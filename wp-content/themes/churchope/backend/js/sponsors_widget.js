jQuery(document).ready( function($) {
    function media_upload(button_class) {
        var _custom_media = true,
            _orig_send_attachment = wp.media.editor.send.attachment;

        $('body').on('click', button_class, function(e) {
            var button_id ='#'+$(this).attr('id');
            var self = $(button_id);
            var send_attachment_bkp = wp.media.editor.send.attachment;
            var button = $(button_id);
            var id = button.attr('id').replace('_button', '');
            _custom_media = true;

            wp.media.editor.send.attachment = function(props, attachment){
                if ( _custom_media  ) {
                    var img_thumb = button.closest('p').find('.custom_media_image');
                    img_thumb.attr('src',attachment.url).css('display','block');

                    var hidden_img_input = button.closest('p').find('.hidden_image_uri');
                    hidden_img_input.val(attachment.url);
                } else {
                    return _orig_send_attachment.apply( button_id, [props, attachment] );
                }
            };

            wp.media.editor.open(button);
            return false;
        });
    }
    media_upload('.custom_media_button.button');
});